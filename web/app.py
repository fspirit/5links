__author__ = 'fs'

import tornado.ioloop
import tornado.web

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")


class TwitterLoginHandler(tornado.web.RequestHandler,
                          tornado.auth.TwitterMixin):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        if self.get_argument("oauth_token", None):
            user = yield self.get_authenticated_user()
            self.write("Authenticated user:")
            self.write(user)
        else:
            yield self.authorize_redirect()

application = tornado.web.Application([
    (r"/", TwitterLoginHandler, { "twitter_consumer_key": "EhT4WyAplOdzTUpVJyEQ",
                                  "twitter_consumer_secret": "VKcT9NhHw2OWzYtTHYLpGjS7PnTJ3y6u8rYzD55ZIQ0"} ),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

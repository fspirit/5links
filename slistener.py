from tweepy.api import API
import logging

__author__ = 'fs'

from tweepy import StreamListener
import json, time, sys
import sqlite3 as lite

my_id = 90892844

class SListener(StreamListener):

    def __init__(self, api = None, fprefix = 'streamer'):
        self.api = api or API()
        self.counter = 0
        self.fprefix = fprefix

    def on_data(self, data):
        # print data
        if 'event' in data:
            # event
            if 'unfavorite' in data:
                return
            if 'favorite' in data:
                event = json.loads(data)
                if event['source']['id'] == my_id and event["target_object"]["entities"]["urls"] != 0:
                    self.on_like(event)
        elif 'in_reply_to_status_id' in data:
            # tweet or retweet
            status = json.loads(data)
            if 'retweeted_status' in status:
                if status['user']['id'] == my_id:
                    self.like_post(status['retweeted_status']['id'])
                else:
                    self.on_status(status['retweeted_status'])
            else:
                self.on_status(status)
        elif 'delete' in data:
            # tweet delete
            delete = json.loads(data)['delete']['status']
            if self.on_delete(delete['id'], delete['user_id']) is False:
                return False
        elif 'limit' in data:
            if self.on_limit(json.loads(data)['limit']['track']) is False:
                return False
        elif 'warning' in data:
            warning = json.loads(data)['warnings']
            print warning['message']
            return False

    def on_status(self, status):

        parsed_data = status

        if "entities" in parsed_data and "urls" in parsed_data["entities"] and len(parsed_data["entities"]["urls"]) != 0:
            post = {
                "post_id" : parsed_data["id"],
                    "liked" : ("retweeted" in parsed_data and parsed_data["retweeted"]) or ("favorited" in parsed_data and parsed_data["favorited"]),
                    "like_count" : parsed_data["retweet_count"],
                    "fav_count" : parsed_data["favorite_count"],
                    "url" : parsed_data["entities"]["urls"][0]["expanded_url"],
                    "friends_likes" : 0,
                    "close_friends_likes" : 0,
                    "created_at" : time.mktime( time.strptime( parsed_data["created_at"],'%a %b %d %H:%M:%S +0000 %Y') )
                    }

            conn = lite.connect('twi_stream.sqlite')

            print type(post['post_id'])

            with conn:

                cur = conn.cursor()

                # check for dupes
                cur.execute("SELECT * FROM POSTS WHERE POST_ID=:post_id", { 'post_id': post['post_id'] })
                result = cur.fetchone()

                if (not result):
                    cur.execute("INSERT INTO POSTS (POST_ID, LIKED, LIKE_COUNT, FAV_COUNT, URL, FRIENDS_LIKES, CLOSE_FRIENDS_LIKES, CREATED_AT) "
                        "VALUES( CAST(:post_id AS INTEGER), :liked, :like_count, :fav_count, :url, :friends_likes, :close_friends_likes, :created_at)",
                        post)
                    conn.commit()

                    print "Tweet inserted."
                    logging.info("Tweet inserted.")

        return

    def on_delete(self, status_id, user_id):
        return

    def on_limit(self, track):
        sys.stderr.write(track + "\n")
        return

    def on_error(self, status_code):
        sys.stderr.write('Error: ' + str(status_code) + "\n")
        logging.error('Error: ' + str(status_code) + "\n")
        return False

    def on_timeout(self):
        sys.stderr.write("Timeout, sleeping for 60 seconds...\n")
        logging.info("Timeout, sleeping for 60 seconds...\n")
        time.sleep(60)
        return

    def like_post(self, post_id):
        conn = lite.connect('twi_stream.sqlite')

        with conn:
            cur = conn.cursor()
            cur.execute("UPDATE POSTS SET LIKED=:like WHERE POST_ID=:post_id",
                        { "like":1,
                          "post_id":post_id })
            conn.commit()

            print "Tweet liked."
            logging.info("Tweet liked.")
        return

    def on_like(self, event):
        self.like_post(event["target_object"]["id"])
        return
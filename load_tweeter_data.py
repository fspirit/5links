import base64
import ssl
import httplib
import operator
from string import join
import time

__author__ = 'fs'

import oauth2 as oauth
import urllib2
import urllib

import json

# API used
# https://api.twitter.com/1.1/friends/ids most probably 1-2 calls, limit: 15/user, 15/app
# https://api.twitter.com/1.1/followers/ids most probably  1-2 calls, limit: 15/user, 15/app
# https://api.twitter.com/1.1/statuses/home_timeline, 4 calls, limit: 15/user
# https://api.twitter.com/1.1/statuses/retweeters/id, up to 800-1200-2400 calls, limit: 15/user, 60/app

access_token_key = "90892844-voAHfoSDL075epBexOGayem1wJ2eTqJ7oyDVxECWW"
access_token_secret = "BM9km7PpZQDUFkrV2r41TX4bTVvxwhl6B4kgjJIbI"

consumer_key = "EhT4WyAplOdzTUpVJyEQ"
consumer_secret = "VKcT9NhHw2OWzYtTHYLpGjS7PnTJ3y6u8rYzD55ZIQ0"

_debug = 0

oauth_token    = oauth.Token(key=access_token_key, secret=access_token_secret)
oauth_consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)

signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

http_handler  = urllib2.HTTPHandler(debuglevel=_debug)
https_handler = urllib2.HTTPSHandler(debuglevel=_debug)

'''
Construct, sign, and open a twitter request
using the hard-coded credentials above.
'''
def twitterreq(url, http_method, parameters):
    req = oauth.Request.from_consumer_and_token(oauth_consumer,
        token=oauth_token,
        http_method=http_method,
        http_url=url,
        parameters=parameters)

    req.sign_request(signature_method_hmac_sha1, oauth_consumer, oauth_token)

    headers = req.to_header()

    if http_method == "POST":
        encoded_post_data = req.to_postdata()
    else:
        encoded_post_data = None
        url = req.to_url()

    opener = urllib2.OpenerDirector()
    opener.add_handler(http_handler)
    opener.add_handler(https_handler)

    response = opener.open(url, encoded_post_data)

    return response



def get_all_ids_from_url(url):

    cursor = -1
    ids = []

    while cursor:
        url_with_cursor = url + "&cursor=" + str(cursor)
        response = twitterreq(url_with_cursor, "GET", [])
        _json = json.load(response)
        if "ids" not in _json:
            break
        ids.extend(_json["ids"])
        cursor = _json[ 'next_cursor' ]

    return ids

def load_tweets():
    max_id = None
    tweeter_data = []

    for x in range(0, 4):
        url = "https://api.twitter.com/1.1/statuses/home_timeline.json?count=200&trim_user=true"

        if max_id:
            url = url + "&max_id=" + str(max_id)

        response = twitterreq(url, "GET", [])
        tweets = json.load(response)

        print len(tweets)

        for tweet in tweets:
            tweet_data = {}

            if "entities" in tweet and "urls" in tweet["entities"] and len(tweet["entities"]["urls"]) != 0:
                tweet_data["expanded_url"] = tweet["entities"]["urls"][0]["expanded_url"]
                tweet_data["author_id"] = tweet["user"]["id_str"]
                tweet_data["retweet_count"] = tweet["retweet_count"]
                tweet_data["liked"] = ("favorited" in tweet and tweet["favorited"]) or ("retweeted" in tweet and tweet["retweeted"])
                tweet_data["fav_count"] = tweet["favorite_count"]
            else:
                continue

            if not max_id or max_id > tweet["id"]:
                max_id = tweet["id"]

            max_id -= 1

            tweet_data["id"] = tweet["id_str"]

            tweeter_data.append(tweet_data)

    return tweeter_data

def fetchsamples():

    print "Loading tweets"
    tweeter_data = load_tweets()

    print "Dumping to json..."
    json.dump(tweeter_data, open("twi_data_temp_dumo.txt", "w"))

    #tweeter_data = json.load(open("twi_data_temp_dumo.txt", "r"))

    print "Loading friends/followers..."

    friends_ids = get_all_ids_from_url("https://api.twitter.com/1.1/friends/ids.json?count=5000&user_id=GUID&stringify_ids=true")
    followers_ids = get_all_ids_from_url("https://api.twitter.com/1.1/followers/ids.json?count=5000&user_id=GUID&stringify_ids=true")

    close_friends_ids = []
    for follower_id in followers_ids:
        if follower_id in friends_ids:
            close_friends_ids.append(follower_id)

    print "Getting bearer token..."

    bearer_token = get_bearer_token()

    print "Getting retweeters for tweets..."
    for tweet in tweeter_data:

        friends_likes = 0
        close_friends_likes = 0

        if (tweet["retweet_count"] != 0):

            print "Retweet count {0}".format(tweet["retweet_count"])
            retweeters_ids = get_retweeters_for_tweet_id(tweet["id"], bearer_token)

            tweet["retweet_count"] = len(retweeters_ids)

            for retweeter_id in retweeters_ids:
                if retweeter_id in friends_ids:
                    friends_likes += 1

            for retweeter_id in retweeters_ids:
                if retweeter_id in close_friends_ids:
                    close_friends_likes += 1

            tweet["friends_likes"] = friends_likes
            tweet["close_friends_likes"] = close_friends_likes



    if len(tweeter_data) == 0:
        return

    print "Writing data to json and csv..."

    json.dump(tweeter_data, open("tweeter_data_json.txt", "w"))

    f = open('tweeter_data.csv', 'w')

    sorted_keys = sorted( tweeter_data[0].keys() )
    f.write( join(sorted_keys, ",") )

    for tweet in tweeter_data:
        sorted_values = [str(x[1]) for x in sorted(tweet.iteritems(), key=operator.itemgetter(0))]
        f.write( join( sorted_values, ",") )

def get_retweeters_for_tweet_id(tweet_id, bearer_token):
    url = "https://api.twitter.com/1.1/statuses/retweeters/ids.json?count=100&id={0}&stringify_ids=true".format(tweet_id)

    #curl --get 'https://api.twitter.com/1.1/statuses/retweeters/ids.json' --data 'count=100&id=341812993820094464&stringify_ids=true' --header 'Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAKCcQgAAAAAAZkLqnRgYAyA5fnx81qVDxYiiwLE%3DqihU2InPfhWOFMFqi9K47FOXozEPN92WqzOgVPDw' --verbose

    cursor = -1
    ids = []

    while cursor:
        url_with_cursor = url + "&cursor=" + str(cursor)

        req = urllib2.Request(url_with_cursor)
        req.add_header("Authorization", "Bearer %s" % bearer_token)

        try:
            resp = urllib2.urlopen(req)
        except urllib2.HTTPError as e:
            if int(e.code) == 429:
                print "Too many requests... Start waiting for 5 mins"
                time.sleep(5 * 60)
                print "Finished waiting, resuming..."
                continue
            else:
                raise

        _json = json.load(resp)
        if "ids" not in _json:
            print "No ids field in response"
            break
        ids.extend(_json["ids"])
        cursor = _json[ 'next_cursor' ]

    print "Got {0} retweeters...".format(len(ids))
    return ids

def get_bearer_token():
    encoded_CONSUMER_KEY = urllib.quote(consumer_key)
    encoded_CONSUMER_SECRET = urllib.quote(consumer_secret)

    concat_consumer_url = encoded_CONSUMER_KEY + ":" + encoded_CONSUMER_SECRET

    host = 'api.twitter.com'
    url = '/oauth2/token/'
    params = urllib.urlencode({'grant_type' : 'client_credentials'})
    req = httplib.HTTPSConnection(host)
    req.putrequest("POST", url)
    req.putheader("Host", host)
    req.putheader("User-Agent", "My Twitter 1.1")
    req.putheader("Authorization", "Basic %s" % base64.b64encode(concat_consumer_url))
    req.putheader("Content-Type" ,"application/x-www-form-urlencoded;charset=UTF-8")
    req.putheader("Content-Length", "29")

    req.endheaders()
    req.send(params)

    resp = req.getresponse()

    body = json.loads(resp.read())
    return body["access_token"]

if __name__ == '__main__':
    fetchsamples()




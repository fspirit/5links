__author__ = 'fs'

import sqlite3 as lite
import datetime
import time
import oauth2 as oauth
import urllib2
import urllib
import json
import httplib
import base64
import logging

access_token_key = "90892844-voAHfoSDL075epBexOGayem1wJ2eTqJ7oyDVxECWW"
access_token_secret = "BM9km7PpZQDUFkrV2r41TX4bTVvxwhl6B4kgjJIbI"

consumer_key = "EhT4WyAplOdzTUpVJyEQ"
consumer_secret = "VKcT9NhHw2OWzYtTHYLpGjS7PnTJ3y6u8rYzD55ZIQ0"

_debug = 0

oauth_token    = oauth.Token(key=access_token_key, secret=access_token_secret)
oauth_consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)

signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

http_handler  = urllib2.HTTPHandler(debuglevel=_debug)
https_handler = urllib2.HTTPSHandler(debuglevel=_debug)

logging.basicConfig(filename='updater.log',
                    level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M')
logging.info("Setting up updater...")

def twitterreq(url, http_method, parameters):

    req = oauth.Request.from_consumer_and_token(oauth_consumer,
        token=oauth_token,
        http_method=http_method,
        http_url=url,
        parameters=parameters)

    req.sign_request(signature_method_hmac_sha1, oauth_consumer, oauth_token)

    headers = req.to_header()

    if http_method == "POST":
        encoded_post_data = req.to_postdata()
    else:
        encoded_post_data = None
        url = req.to_url()

    opener = urllib2.OpenerDirector()
    opener.add_handler(http_handler)
    opener.add_handler(https_handler)

    response = opener.open(url, encoded_post_data)

    return response

def get_all_ids_from_url(url):

    cursor = -1
    ids = []

    while cursor:
        url_with_cursor = url + "&cursor=" + str(cursor)
        response = twitterreq(url_with_cursor, "GET", [])
        _json = json.load(response)
        if "ids" not in _json:
            break
        ids.extend(_json["ids"])
        cursor = _json[ 'next_cursor' ]

    return ids

def get_retweeters_for_tweet_id(tweet_id, bearer_token):
    url = "https://api.twitter.com/1.1/statuses/retweeters/ids.json?count=100&id={0}&stringify_ids=true".format(tweet_id)

    #curl --get 'https://api.twitter.com/1.1/statuses/retweeters/ids.json' --data 'count=100&id=341812993820094464&stringify_ids=true' --header 'Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAKCcQgAAAAAAZkLqnRgYAyA5fnx81qVDxYiiwLE%3DqihU2InPfhWOFMFqi9K47FOXozEPN92WqzOgVPDw' --verbose

    cursor = -1
    ids = []

    while cursor:
        url_with_cursor = url + "&cursor=" + str(cursor)

        req = urllib2.Request(url_with_cursor)
        req.add_header("Authorization", "Bearer %s" % bearer_token)

        try:
            resp = urllib2.urlopen(req)
        except urllib2.HTTPError as e:
            logging.error("Error %s" % e.code)
            if int(e.code) == 429:
                print "Too many requests... Start waiting for 5 mins"
                time.sleep(5 * 60)
                print "Finished waiting, resuming..."
                continue
            else:
                raise

        _json = json.load(resp)
        if "ids" not in _json:
            print "No ids field in response"
            break
        ids.extend(_json["ids"])
        cursor = _json[ 'next_cursor' ]

    print "Got {0} retweeters...".format(len(ids))
    logging.info("Got {0} retweeters...".format(len(ids)))
    return ids

def get_bearer_token():
    encoded_CONSUMER_KEY = urllib.quote(consumer_key)
    encoded_CONSUMER_SECRET = urllib.quote(consumer_secret)

    concat_consumer_url = encoded_CONSUMER_KEY + ":" + encoded_CONSUMER_SECRET

    host = 'api.twitter.com'
    url = '/oauth2/token/'
    params = urllib.urlencode({'grant_type' : 'client_credentials'})
    req = httplib.HTTPSConnection(host)
    req.putrequest("POST", url)
    req.putheader("Host", host)
    req.putheader("User-Agent", "My Twitter 1.1")
    req.putheader("Authorization", "Basic %s" % base64.b64encode(concat_consumer_url))
    req.putheader("Content-Type" ,"application/x-www-form-urlencoded;charset=UTF-8")
    req.putheader("Content-Length", "29")

    req.endheaders()
    req.send(params)

    resp = req.getresponse()

    body = json.loads(resp.read())
    return body["access_token"]

def main():

    print "Loading friends/followers..."

    friends_ids = get_all_ids_from_url("https://api.twitter.com/1.1/friends/ids.json?count=5000&user_id=GUID&stringify_ids=true")
    followers_ids = get_all_ids_from_url("https://api.twitter.com/1.1/followers/ids.json?count=5000&user_id=GUID&stringify_ids=true")

    close_friends_ids = []
    for follower_id in followers_ids:
        if follower_id in friends_ids:
            close_friends_ids.append(follower_id)

    print "Getting bearer token..."

    bearer_token = get_bearer_token()

    conn = lite.connect('twi_stream.sqlite')

    conn.row_factory = lite.Row

    with conn:

        cur = conn.cursor()

        cutoff_date = datetime.datetime.utcnow() - datetime.timedelta(days=1)
        cur.execute("SELECT * FROM POSTS WHERE CREATED_AT > %d" % time.mktime(cutoff_date.timetuple()))

        conn.commit()

        tweets = cur.fetchall()

        logging.info("Fetched %d tweets to update." % len(tweets))

        for tweet in [dict(row) for row in tweets]:

            friends_likes = 0
            close_friends_likes = 0

            # get updated tweet

            print "Updating tweet with id %d" % tweet["POST_ID"]
            logging.info("Updating tweet with id %d" % tweet["POST_ID"])

            try:
                resp = twitterreq('http://api.twitter.com/1.1/statuses/show.json?id=%d&include_entities=false&trim_user=true&include_my_retweet=false' % tweet["POST_ID"],
                                  "GET",[])
            except urllib2.HTTPError as e:
                logging.error("Http error %s" % e.code)
                if int(e.code) == 429:
                    logging.info("Too many requests... Start waiting for 5 mins")
                    print "Too many requests... Start waiting for 5 mins"
                    time.sleep(5 * 60)
                    logging.info("Finished waiting, resuming...")
                    print "Finished waiting, resuming..."
                    continue
                if int(e.code) == 404:
                    continue
                else:
                    raise

            updated_tweet = json.load(resp)

            print updated_tweet

            tweet["LIKE_COUNT"] = updated_tweet["retweet_count"];
            tweet["FAV_COUNT"] = updated_tweet["favorite_count"];

            if (tweet["LIKE_COUNT"] != 0):

                print "Retweet count {0}".format(tweet["LIKE_COUNT"])
                retweeters_ids = get_retweeters_for_tweet_id(tweet["POST_ID"], bearer_token)

                tweet["LIKE_COUNT"] = len(retweeters_ids)

                for retweeter_id in retweeters_ids:
                    if retweeter_id in friends_ids:
                        friends_likes += 1

                for retweeter_id in retweeters_ids:
                    if retweeter_id in close_friends_ids:
                        close_friends_likes += 1

                tweet["FRIENDS_LIKES"] = friends_likes
                tweet["CLOSE_FRIENDS_LIKES"] = close_friends_likes


            cur.execute("UPDATE POSTS SET LIKE_COUNT=:LIKE_COUNT, FRIENDS_LIKES=:FRIENDS_LIKES, "
                        "CLOSE_FRIENDS_LIKES=:CLOSE_FRIENDS_LIKES, FAV_COUNT=:FAV_COUNT WHERE POST_ID=:POST_ID", tweet)
            conn.commit()

            logging.info("Tweet info updated.")

    return

if __name__ == '__main__':
    main()